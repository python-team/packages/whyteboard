��    t      �  �   \      �	     �	     �	     �	     �	     �	  	   �	     
     
     
     !
     '
     5
     >
     D
     Q
     W
  	   j
     t
     �
     �
     �
  	   �
     �
     �
  
   �
     �
     �
     �
     �
     �
     �
                    ,  	   L     V  '   k     �     �  
   �     �     �     �     �     �               *     1     7     O     ]     z     �     �     �     �     �     �  
   �     �  
   �     �     �     �          
                    %     .  
   3     >  	   J     T     [     `     f     r          �     �     �     �     �     �  	   �     �     �            
        #     /     5     =  P   B  
   �  
   �     �     �     �     �     �     �  #   �  !        8     L     ]  '   c     �     �  �  �     A     M  	   `     j  	   s     }     �  
   �     �  	   �     �     �     �     �     �     �               4     8     F     O  	   \     f     n          �     �     �     �     �     �     �     �  &        ;     L  #   j  
   �     �     �     �     �     �     �     �          #     5     :     C     _     p     �     �     �     �     �     �     �  
   �     �     �            	        &     /     7     C  
   I     T     `     h     x  
   �  
   �  	   �  	   �     �     �     �  	   �     �  
   �               -  
   L     W     s     �     �     �     �     �     �     �  C   �     !     -     6  
   P     [     h     t     �  +   �     �     �     �  	     (   !     J  	   ]     `   m       s                 !      Y   W      ,           .      '   d   ^   :   "   a       L   j             n               C   =      I   (              t      \           k          #   *   R       ;   P                 %       T   J   O   @   A       K   -   
   4          	   N      [       U   <       2   ]           _   6       c   1   b         e   f   q   H   3   9   V   r      o          M   0   X       &   F             Q       B       i          8   E      h              /   g         5   )          p       Z           7   G   +   l       S       $      ?   D   >            &About &Add New Point &Apply &Cancel &Close &Contents &Copy &Delete &Don't Save &Edit &Edit Note... &Edit... &File &Full Screen &Help &History Viewer... &Image... &Import File &OK &Open... &Paste &Print... &Quit &Redo &Rename... &Report a Problem &Save &Select &Status Bar &Toolbar &Translate Whyteboard &Undo &View A preview of your current tool Adds a new point to the Polygon All files All suppported files An error has occured - please report it Arabic Arrow As &PDF... Audio Files Bitmap Select Cancelling... Change your preferences Check for &Updates... Choose Your Language: Choose a directory Circle Color Connecting to server... Converting... Could not connect to server. Czech Draw a rectangle Draw a straight line Draw an oval shape Dutch Ellipse English Enter text Eraser Eyedropper French General German Height: High Highest Icons Italian Japanese Line Loading... New &Window New Sheet Normal Note Notes Open a File Open file... Page Set&up Page: Pen Preferences Print Pre&view Print Preview Print the current page Rectangle Rename the current sheet Resize Canvas Retry Save &As... Save File? Select Font Sheet Spanish Text There was a problem printing.
Perhaps your current printer is not set correctly? Thickness: Thumbnails Undo the last operation Untitled Update Updates Updating Thumbnails View View Whyteboard in full-screen mode View information about Whyteboard View the status bar View the toolbar Welsh Whyteboard doesn't support the filetype Whyteboard file  Width: Project-Id-Version: whyteboard
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2010-09-17 15:46+0100
PO-Revision-Date: 2010-04-26 18:08+0000
Last-Translator: Lukáš Machyán <Unknown>
Language-Team: Czech <cs@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-09-17 15:48+0000
X-Generator: Launchpad (build Unknown)
 &O aplikaci Přid&at nový bod &Použít &Zrušit &Zavřít O&bsah &Kopírovat O&dstranit Neuklá&dat &Editovat &Upravit poznámku... &Upravit... &Soubor &Celá obrazovka &Nápověda &Zobrazení historie... &Obrázek... &Importování souboru &OK &Otevřít... &Vložit &Tisknout... &Ukončit &Vpřed &Přejmenovat... &Nahlásit problém &Uložit &Vybrat &Stavový řádek &Nástrojová lišta &Přeložit Whyteboard &Zpět &Zobrazení Náhled aktuálního nástroje Přidává nový bod k mnohoúhelníku Všechny soubory Všechny podporované soubory Nastala chyba - nahlaste ji prosím Arabština Šipka Jako &PDF... Zvukové soubory Vybrat bitmap Ruší se... Změna svého nastavení Kontrolovat &Aktualizace... Vyberte svůj jazyk: Vyberte adresář Kruh Barevný Připojování k serveru... Převádění... Nelze se připojit k serveru Český Kreslit obdélník Kreslit rovnou čáru Kreslit ovál Holandština Elipsa Angličtina Vlož text Guma Kapátko Francouzština Obecné Německý Výška: Vysoký Nejvyšší Ikony Italština Japonština Řádka Načítá se... Nové &okno Nový list Normální Poznámka Poznámky Otevřít soubor Otevřít soubor... Na&stavení stránky Stránka: Pero Předvolby Náhle&d před tiskem Náhled před tiskem Vytisknout aktuální stránku Obdélník Přejmenuje aktuální list Změna velikosti plátna Opakovat Uložit j&ako... Uložit soubor? Vybrat písmo List Španělština Text Nastala chyba během tisku.
Je Vaše tiskárna správně nastavena? Tloušťka: Náhledy Vrátit poslední operaci Bez názvu Aktualizovat Aktualizace Aktualizují se náhledy Zobrazit Zobrazit Whyteboard v módu celé obrazovky Zobrazit informace o Whyteboard Zobrazit stavový řádek Zobrazit nástrojové menu Velština Whyteboard nepodporuje tento typ souboru Whyteboard soubor  Šířka: 